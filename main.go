package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"image"
	"image/jpeg"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"strconv"

	"github.com/gorilla/mux"
	"github.com/k0kubun/pp"
	"github.com/nfnt/resize"
)

// Galleryitem represent an image
type Galleryitem struct {
	ID      int
	Name    string
	RelPath string
	Likes   int
	Comment string
}

var fFolder = "images"

var galleryitems map[int]Galleryitem
var port = "8081"

var cache map[int][]byte

func main() {

	flag.StringVar(&fFolder, "imgdir", "images", "image directory")
	flag.Parse()
	var err error
	galleryitems, err = getFiles(fFolder)
	if err != nil {
		log.Fatal(err)
	}
	cache = make(map[int][]byte)
	r := mux.NewRouter()
	r.HandleFunc("/images", listImagesHandler)
	r.HandleFunc("/images/{id}", imageHandler)
	r.HandleFunc("/images/{id}/img", thumbHandler)
	pp.Println(galleryitems)

	fmt.Println("Starting server on port ", port)
	log.Fatal(http.ListenAndServe(":"+port, r))
}

func listImagesHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	json.NewEncoder(w).Encode(galleryitems)
}

func imageHandler(w http.ResponseWriter, r *http.Request) {
	id, _ := strconv.Atoi(mux.Vars(r)["id"])
	json.NewEncoder(w).Encode(galleryitems[id])
}

func getFiles(folder string) (g map[int]Galleryitem, err error) {
	files, err := ioutil.ReadDir(folder)

	if err != nil {
		err = fmt.Errorf("error reading imagedir (%s): %s", folder, err)
		return nil, err
	}
	g = make(map[int]Galleryitem)

	for i, file := range files {
		p := fmt.Sprint(filepath.Join(folder, file.Name()))
		g[i] = Galleryitem{
			ID:      i,
			Name:    file.Name(),
			RelPath: p,
		}
	}
	return g, err
}

func thumbHandler(w http.ResponseWriter, r *http.Request) {
	id, _ := strconv.Atoi(mux.Vars(r)["id"])

	if _, ok := cache[id]; ok {
		w.Write(cache[id])
		return
	}
	var thumbnail bytes.Buffer
	out := io.MultiWriter(w, &thumbnail)

	generateThumbnail(galleryitems[id].RelPath, out)

	cache[id] = thumbnail.Bytes()

}

func generateThumbnail(original string, out io.Writer) {
	reader, err := os.Open(original)
	if err != nil {
		log.Fatal(err)
	}
	defer reader.Close()

	originalImg, _, err := image.Decode(reader)
	if err != nil {
		log.Printf("error decoding image:%s", err)
		return
	}

	// resize it
	resizedImg := resize.Resize(300, 0, originalImg, resize.Lanczos3)

	err = jpeg.Encode(out, resizedImg, nil)
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
}

FROM golang:latest
COPY . /src
CMD cd /src && go build && cp imgapi /mnt/artifacts

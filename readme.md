# imgapi

A very simple image api for demo purpose

## usage

```bash

# check out outside of GOPATH ( project uses modules, introduced in go1.11)
# go build
./imgapi -imgdir images #(where images is a folder containing images)

#or
go run main.go  -imgdir images/

# build in docker:
mkdir artifacts
docker build -t kkgobuild .
docker run --rm -v $(pwd)/artifacts:/mnt/artifacts kkgobuild:latest

#see binary in the artifacts folder
# todo: owner:group on artifact.

# service will listen on localhost:8081
```

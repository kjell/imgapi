module gitlab.com/kjell/imgapi

require (
	github.com/gorilla/mux v1.6.2
	github.com/k0kubun/pp v2.3.0+incompatible
	github.com/mattn/go-colorable v0.0.9 // indirect
	github.com/mattn/go-isatty v0.0.4 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
)
